# KeyboardControl

[![CI Status](http://img.shields.io/travis/mikhailmulyar/KeyboardControl.svg?style=flat)](https://travis-ci.org/mikhailmulyar/KeyboardControl)
[![Version](https://img.shields.io/cocoapods/v/KeyboardControl.svg?style=flat)](http://cocoapods.org/pods/KeyboardControl)
[![License](https://img.shields.io/cocoapods/l/KeyboardControl.svg?style=flat)](http://cocoapods.org/pods/KeyboardControl)
[![Platform](https://img.shields.io/cocoapods/p/KeyboardControl.svg?style=flat)](http://cocoapods.org/pods/KeyboardControl)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

KeyboardControl is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "KeyboardControl"
```

## Author

mikhailmulyar, mulyarm@gmail.com

## License

KeyboardControl is available under the MIT license. See the LICENSE file for more info.
