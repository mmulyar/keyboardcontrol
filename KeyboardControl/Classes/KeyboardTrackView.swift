//
// Created by Mikhail Mulyar on 02/10/16.
// Copyright (c) 2016 Mikhail Mulyar. All rights reserved.
//

import UIKit


class KeyboardTrackView: UIView {

    var positionChangedCallback: ((_ frame: CGRect) -> Void)?
    var observedView:            UIView?

    deinit {
        if let observedView = self.observedView {
            observedView.removeObserver(self, forKeyPath: #keyPath(UIView.frame))
            observedView.removeObserver(self, forKeyPath: #keyPath(UIView.center))
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }

    fileprivate func commonInit() {
        self.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.isUserInteractionEnabled = false
        self.backgroundColor = UIColor.clear
        self.isHidden = true
    }

    var preferredSize: CGSize = .zero {
        didSet {
            if oldValue != self.preferredSize {
                self.invalidateIntrinsicContentSize()
                self.window?.setNeedsLayout()
            }
        }
    }

    override var intrinsicContentSize: CGSize {
        return self.preferredSize
    }

    override func willMove(toSuperview newSuperview: UIView?) {
        if let observedView = self.observedView {
            observedView.removeObserver(self, forKeyPath: #keyPath(UIView.frame))
            observedView.removeObserver(self, forKeyPath: #keyPath(UIView.center))
            self.observedView = nil
        }

        if let newSuperview = newSuperview {
            newSuperview.addObserver(self, forKeyPath: #keyPath(UIView.frame), options: [.new, .old], context: nil)
            newSuperview.addObserver(self, forKeyPath: #keyPath(UIView.center), options: [.new, .old], context: nil)
            self.observedView = newSuperview
        }

        super.willMove(toSuperview: newSuperview)
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        if let s = superview {
            self.positionChangedCallback?(s.frame)
        }
    }


    override func observeValue(forKeyPath keyPath: String?,
                               of object: Any?,
                               change: [NSKeyValueChangeKey: Any]?,
                               context: UnsafeMutableRawPointer?) {
        guard let superview = self.superview else {
            return
        }

        let object = object as AnyObject

        if object === superview {
            guard let sChange = change else {
                return
            }
            if (keyPath == #keyPath(UIView.center)) {
                let oldCenter = (sChange[NSKeyValueChangeKey.oldKey] as! NSValue).cgPointValue
                let newCenter = (sChange[NSKeyValueChangeKey.newKey] as! NSValue).cgPointValue
                if oldCenter != newCenter {
                    self.positionChangedCallback?(superview.frame)
                }
            }
            else if (keyPath == #keyPath(UIView.frame)) {
                let oldFrame = (sChange[NSKeyValueChangeKey.oldKey] as! NSValue).cgRectValue
                let newFrame = (sChange[NSKeyValueChangeKey.newKey] as! NSValue).cgRectValue
                if oldFrame != newFrame {
                    self.positionChangedCallback?(superview.frame)
                }
            }
        }
    }
}

