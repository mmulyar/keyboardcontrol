//
// Created by Mikhail Mulyar on 02/10/16.
// Copyright (c) 2016 Mikhail Mulyar. All rights reserved.
//

import UIKit
import Foundation


public typealias KeyboardDidMoveBlock = ((_ keyboardFrame: CGRect, _ opening: Bool, _ closing: Bool) -> Void)?

private let frameKeyPath = "frame"


class KeyboardDidMoveBlockWrapper {
    var closure: KeyboardDidMoveBlock

    init(_ closure: KeyboardDidMoveBlock) {
        self.closure = closure
    }
}


class PreviousKeyboardRectWrapper {

    var closure: CGRect

    init(_ closure: CGRect) {
        self.closure = closure
    }
}


@inline(__always) func AnimationOptionsForCurve(_ curve: UIViewAnimationCurve) -> UIViewAnimationOptions {

    return UIViewAnimationOptions.init(rawValue: UInt(curve.rawValue))
}


public extension UIView {

// MARK: - Public Properties

    dynamic var keyboardTriggerOffset: CGFloat {

        get {
            guard let triggerOffset: CGFloat = objc_getAssociatedObject(self, &AssociatedKeys.DescriptiveTriggerOffset) as? CGFloat else {
                return 0
            }
            return triggerOffset
        }

        set {
            self.willChangeValue(forKey: "keyboardTriggerOffset");
            objc_setAssociatedObject(self,
                                     &AssociatedKeys.DescriptiveTriggerOffset,
                                     newValue as CGFloat,
                                     .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            self.didChangeValue(forKey: "keyboardTriggerOffset");

            //TODO ISSUE 1 check animations
            //self.keyboardTrackView?.preferredSize = CGSizeMake(self.frame.size.width, newValue)
        }
    }

// MARK: - Public Methods

    func isKeyboardOpened() -> Bool {
        return self.keyboardOpened
    }

    func addKeyboardControlForInputView(_ inputView: UIView,
                                        interactiveDismiss: Bool,
                                        actionHandler: KeyboardDidMoveBlock) {

        self.panning = interactiveDismiss
        self.addKeyboardControl(inputView,
                                actionHandler: actionHandler)
    }

    func keyboardFrameInView() -> CGRect {

        if let activityView = self.keyboardActiveView {

            let keyboardFrameInView = self.convert(activityView.frame, from: activityView.superview)
            return keyboardFrameInView
        }
        else {
            let keyboardFrameInView = CGRect(x: 0, y: UIScreen.main.bounds.size.height, width: 0, height: 0)
            return keyboardFrameInView
        }
    }

    func removeKeyboardControl() {

        // Unregister for text input notifications
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UITextFieldTextDidBeginEditing, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UITextViewTextDidBeginEditing, object: nil)

        // Unregister for keyboard notifications
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardDidShow, object: nil)

        // For the sake of 4.X compatibility
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "UIKeyboardWillChangeFrameNotification"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "UIKeyboardDidChangeFrameNotification"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardDidHide, object: nil)

        // Unregister any gesture recognizer
        if let panRecognizer = self.keyboardPanRecognizer {
            self.removeGestureRecognizer(panRecognizer)
        }

        // Release a few properties
        self.keyboardTrackView?.keyboardDidMoveBlock = nil;
        self.keyboardTrackView = nil;
        self.keyboardDidMoveBlock = nil;
        self.keyboardActiveInput = nil;
        self.keyboardPanRecognizer = nil;
    }

    func hideKeyboard() {

        if self.keyboardActiveView != nil {
            self.keyboardActiveView?.isUserInteractionEnabled = false
            self.keyboardActiveInput?.resignFirstResponder()
        }
    }
}


extension UIView: UIGestureRecognizerDelegate {


    fileprivate struct AssociatedKeys {
        static var DescriptiveTriggerOffset = "DescriptiveTriggerOffset"

        static var UIViewKeyboardTriggerOffset = "UIViewKeyboardTriggerOffset";
        static var UIViewKeyboardDidMoveFrameBasedBlock = "UIViewKeyboardDidMoveFrameBasedBlock";
        static var UIViewKeyboardActiveInput = "UIViewKeyboardActiveInput";
        static var UIViewKeyboardPanRecognizer = "UIViewKeyboardPanRecognizer";
        static var UIViewIsPanning = "UIViewIsPanning";
        static var UIViewKeyboardOpened = "UIViewKeyboardOpened";
        static var UIViewKeyboardTrackView = "UIViewKeyboardTrackView";
    }


    fileprivate var keyboardWillRecede: Bool {

        get {

            guard let activityView = self.keyboardActiveView else { return false }
            guard let activityViewSuperView = activityView.superview else { return false }
            guard let panGesture = self.keyboardPanRecognizer else { return false }

            let keyboardViewHeight   = activityView.bounds.size.height
            let keyboardWindowHeight = activityViewSuperView.bounds.size.height;
            let touchLocationInKeyboardWindow = panGesture.location(in: activityViewSuperView)
            let thresholdHeight               = keyboardWindowHeight - keyboardViewHeight - self.keyboardTriggerOffset + 44.0
            let velocity                      = panGesture.velocity(in: activityView)

            return touchLocationInKeyboardWindow.y >= thresholdHeight && velocity.y >= 0
        }
    }

    fileprivate var keyboardDidMoveBlock: KeyboardDidMoveBlock {

        get {
            guard let cl = objc_getAssociatedObject(self, &AssociatedKeys.UIViewKeyboardDidMoveFrameBasedBlock) as? KeyboardDidMoveBlockWrapper else {
                return nil
            }
            return cl.closure
        }

        set {
            self.willChangeValue(forKey: "frameBasedKeyboardDidMoveBlock")
            objc_setAssociatedObject(self,
                                     &AssociatedKeys.UIViewKeyboardDidMoveFrameBasedBlock,
                                     KeyboardDidMoveBlockWrapper(newValue),
                                     .OBJC_ASSOCIATION_RETAIN)
            self.didChangeValue(forKey: "frameBasedKeyboardDidMoveBlock")
        }
    }

    fileprivate var keyboardActiveInput: UIResponder? {

        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.UIViewKeyboardActiveInput) as? UIResponder
        }

        set {
            if let newValue: UIResponder = newValue {
                self.willChangeValue(forKey: "keyboardActiveInput")
                objc_setAssociatedObject(self,
                                         &AssociatedKeys.UIViewKeyboardActiveInput,
                                         newValue as UIResponder,
                                         .OBJC_ASSOCIATION_RETAIN)
                self.didChangeValue(forKey: "keyboardActiveInput")
            }
        }
    }

    fileprivate var keyboardActiveView: UIView? {
        get {
            return self.keyboardTrackView?.superview
        }
    }
    fileprivate var keyboardTrackView:  KeyboardTrackView? {

        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.UIViewKeyboardTrackView) as? KeyboardTrackView
        }

        set {
            if let newValue: KeyboardTrackView = newValue {
                self.willChangeValue(forKey: "keyboardTrackView")
                objc_setAssociatedObject(self,
                                         &AssociatedKeys.UIViewKeyboardTrackView,
                                         newValue as KeyboardTrackView,
                                         .OBJC_ASSOCIATION_RETAIN)
                self.didChangeValue(forKey: "keyboardTrackView")
            }
        }
    }

    fileprivate var needAdditionalGestureRecognizer: Bool {

        //TODO ISSUE 1 check animations
        //have some lags in animation when keyboardOffset > 0, commented for now
//		if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_7_0 {
//			if self.panning && self.respondsToSelector(Selector("keyboardDismissMode")) {
//				if let _ = self as? UIScrollView {
//					return false
//				}
//			}
//		}

        return true
    }

    fileprivate var panning: Bool {

        get {
            guard let isPanningNumber: NSNumber = objc_getAssociatedObject(self, &AssociatedKeys.UIViewIsPanning) as? NSNumber else {
                return false
            }
            return isPanningNumber.boolValue
        }

        set {
            self.willChangeValue(forKey: "panning");
            objc_setAssociatedObject(self,
                                     &AssociatedKeys.UIViewIsPanning,
                                     NSNumber(value: newValue as Bool),
                                     .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            self.didChangeValue(forKey: "panning");
        }
    }

    fileprivate var keyboardPanRecognizer: UIPanGestureRecognizer? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.UIViewKeyboardPanRecognizer) as? UIPanGestureRecognizer
        }

        set {
            if let newValue: UIPanGestureRecognizer = newValue {
                self.willChangeValue(forKey: "keyboardPanRecognizer")
                objc_setAssociatedObject(self,
                                         &AssociatedKeys.UIViewKeyboardPanRecognizer,
                                         newValue as UIPanGestureRecognizer,
                                         .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
                self.didChangeValue(forKey: "keyboardPanRecognizer")
            }
        }
    }

    fileprivate var keyboardOpened: Bool {
        get {
            guard let isKeyboardOpenedNumber: NSNumber = objc_getAssociatedObject(self, &AssociatedKeys.UIViewKeyboardOpened) as? NSNumber else {
                return false
            }
            return isKeyboardOpenedNumber.boolValue
        }

        set {
            self.willChangeValue(forKey: "keyboardOpened");
            objc_setAssociatedObject(self,
                                     &AssociatedKeys.UIViewKeyboardOpened,
                                     NSNumber(value: newValue as Bool),
                                     .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            self.didChangeValue(forKey: "keyboardOpened");
        }
    }

    // MARK: Add Keyboard Control

    fileprivate func addKeyboardControl(_ inputView: UIView,
                                        actionHandler: KeyboardDidMoveBlock) {

//		if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_7_0 {
//			if self.panning && self.respondsToSelector(Selector("keyboardDismissMode")) {
//				if let scrollView = self as? UIScrollView {
//					scrollView.keyboardDismissMode = .Interactive
//				}
//			}
//		}

        self.setInputView(inputView)

        self.keyboardDidMoveBlock = actionHandler

        // Register for text input notifications
        NotificationCenter.default
                .addObserver(self,
                             selector: #selector(UIView.responderDidBecomeActive(_:)),
                             name: NSNotification.Name.UITextFieldTextDidBeginEditing,
                             object: nil
        )

        NotificationCenter.default
                .addObserver(self,
                             selector: #selector(UIView.responderDidBecomeActive(_:)),
                             name: NSNotification.Name.UITextViewTextDidBeginEditing,
                             object: nil
        )

        // Register for keyboard notifications
        NotificationCenter.default
                .addObserver(self,
                             selector: #selector(UIView.inputKeyboardWillShow(_:)),
                             name: NSNotification.Name.UIKeyboardWillShow,
                             object: nil
        )

        NotificationCenter.default
                .addObserver(self, selector: #selector(UIView.inputKeyboardDidShow), name: NSNotification.Name.UIKeyboardDidShow, object: nil)

        // For the sake of 4.X compatibility
        NotificationCenter.default
                .addObserver(self,
                             selector: #selector(UIView.inputKeyboardWillChangeFrame(_:)),
                             name: NSNotification.Name(rawValue: "UIKeyboardWillChangeFrameNotification"),
                             object: nil
        )

        NotificationCenter.default
                .addObserver(self,
                             selector: #selector(UIView.inputKeyboardDidChangeFrame),
                             name: NSNotification.Name(rawValue: "UIKeyboardDidChangeFrameNotification"),
                             object: nil
        )

        NotificationCenter.default
                .addObserver(self,
                             selector: #selector(UIView.inputKeyboardWillHide(_:)),
                             name: NSNotification.Name.UIKeyboardWillHide,
                             object: nil
        )

        NotificationCenter.default
                .addObserver(self, selector: #selector(UIView.inputKeyboardDidHide), name: NSNotification.Name.UIKeyboardDidHide, object: nil)
    }

    // MARK: - Input Notifications

    func responderDidBecomeActive(_ notification: Notification) {
        self.keyboardActiveInput = notification.object as? UIResponder
        self.inputKeyboardDidShow()
    }

    // MARK: - Keyboard Notifications

    func inputKeyboardWillShow(_ notification: Notification) {

        guard let userInfo = notification.userInfo else {
            return
        }

        var keyboardEndFrameWindow: CGRect = CGRect()
        (userInfo[UIKeyboardFrameEndUserInfoKey] as AnyObject).getValue(&keyboardEndFrameWindow)

        var keyboardTransitionDuration: Double = Double()
        (userInfo[UIKeyboardAnimationDurationUserInfoKey] as AnyObject).getValue(&keyboardTransitionDuration)

        var keyboardTransitionAnimationCurve: UIViewAnimationCurve = UIViewAnimationCurve.easeInOut
        (userInfo[UIKeyboardAnimationCurveUserInfoKey] as AnyObject).getValue(&keyboardTransitionAnimationCurve)

        self.keyboardOpened = true

        UIView.animate(withDuration: keyboardTransitionDuration,
                       delay: 0,
                       options: [AnimationOptionsForCurve(keyboardTransitionAnimationCurve), .beginFromCurrentState],
                       animations: {
                           if self.keyboardDidMoveBlock != nil && !keyboardEndFrameWindow.isNull {
                               let fixFactor  = (self.keyboardTrackView?.frame.size.height ?? 0)
                               let fixedFrame = CGRect(x: keyboardEndFrameWindow.origin.x,
                                                       y: keyboardEndFrameWindow.origin.y + fixFactor,
                                                       width: keyboardEndFrameWindow.size.width,
                                                       height: keyboardEndFrameWindow.size.height - fixFactor)
                               self.keyboardDidMoveBlock?(fixedFrame,
                                                          true,
                                                          false
                               )
                           }
                       }) {
            (finished: Bool) in
            if self.panning && self.needAdditionalGestureRecognizer {
                self.keyboardPanRecognizer = UIPanGestureRecognizer(target: self, action: #selector(UIView.panGestureDidChange(_:)))
                self.keyboardPanRecognizer?.minimumNumberOfTouches = 1
                self.keyboardPanRecognizer?.delegate = self
                self.keyboardPanRecognizer?.cancelsTouchesInView = false

                guard let panGesture = self.keyboardPanRecognizer else {
                    return
                }
                self.addGestureRecognizer(panGesture)
            }
        }
    }

    func inputKeyboardWillChangeFrame(_ notification: Notification) {

        guard let userInfo = notification.userInfo else {
            return
        }

        var keyboardEndFrameWindow: CGRect = CGRect()
        (userInfo[UIKeyboardFrameEndUserInfoKey] as AnyObject).getValue(&keyboardEndFrameWindow)

        var keyboardTransitionDuration: Double = Double()
        (userInfo[UIKeyboardAnimationDurationUserInfoKey] as AnyObject).getValue(&keyboardTransitionDuration)

        var keyboardTransitionAnimationCurve: UIViewAnimationCurve = UIViewAnimationCurve.easeInOut
        (userInfo[UIKeyboardAnimationCurveUserInfoKey] as AnyObject).getValue(&keyboardTransitionAnimationCurve)

        UIView.animate(withDuration: keyboardTransitionDuration,
                       delay: 0,
                       options: [AnimationOptionsForCurve(keyboardTransitionAnimationCurve), .beginFromCurrentState],
                       animations: {
                           if self.keyboardDidMoveBlock != nil && !keyboardEndFrameWindow.isNull {
                               let fixFactor  = (self.keyboardTrackView?.frame.size.height ?? 0)
                               let fixedFrame = CGRect(x: keyboardEndFrameWindow.origin.x,
                                                       y: keyboardEndFrameWindow.origin.y + fixFactor,
                                                       width: keyboardEndFrameWindow.size.width,
                                                       height: keyboardEndFrameWindow.size.height - fixFactor)
                               self.keyboardDidMoveBlock?(fixedFrame,
                                                          false,
                                                          false
                               )
                           }
                       }, completion: nil)
    }

    func inputKeyboardDidChangeFrame() {
        // Nothing to see here
    }

    func inputKeyboardDidShow() {
        if self.keyboardActiveInput == nil {
            self.keyboardActiveInput = self.recursiveFindFirstResponder(self)
        }
    }

    func inputKeyboardWillHide(_ notification: Notification) {

        guard let userInfo = notification.userInfo else {
            return
        }

        var keyboardEndFrameWindow: CGRect = CGRect()
        (userInfo[UIKeyboardFrameEndUserInfoKey] as AnyObject).getValue(&keyboardEndFrameWindow)

        var keyboardTransitionDuration: Double = Double()
        (userInfo[UIKeyboardAnimationDurationUserInfoKey] as AnyObject).getValue(&keyboardTransitionDuration)

        var keyboardTransitionAnimationCurve: UIViewAnimationCurve = UIViewAnimationCurve.easeInOut
        (userInfo[UIKeyboardAnimationCurveUserInfoKey] as AnyObject).getValue(&keyboardTransitionAnimationCurve)

        UIView.animate(withDuration: keyboardTransitionDuration,
                       delay: 0,
                       options: [AnimationOptionsForCurve(keyboardTransitionAnimationCurve), .beginFromCurrentState],
                       animations: {

                           if self.keyboardDidMoveBlock != nil && !keyboardEndFrameWindow.isNull {
                               let fixFactor  = (self.keyboardTrackView?.frame.size.height ?? 0)
                               let fixedFrame = CGRect(x: keyboardEndFrameWindow.origin.x,
                                                       y: keyboardEndFrameWindow.origin.y + fixFactor,
                                                       width: keyboardEndFrameWindow.size.width,
                                                       height: keyboardEndFrameWindow.size.height - fixFactor)
                               self.keyboardDidMoveBlock?(fixedFrame,
                                                          false,
                                                          true
                               )
                           }
                       }) {
            (finished: Bool) in
            if let panRecognizer = self.keyboardPanRecognizer {
                self.removeGestureRecognizer(panRecognizer)
            }
            self.keyboardPanRecognizer = nil
        }
    }

    func inputKeyboardDidHide() {
        self.keyboardActiveInput = nil
        self.keyboardOpened = false
    }

    // MARK: - Touches Management

    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer,
                                  shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {

        if gestureRecognizer == self.keyboardPanRecognizer || otherGestureRecognizer == self.keyboardPanRecognizer {
            return true
        }
        else {
            return false
        }
    }

    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {

        if gestureRecognizer == self.keyboardPanRecognizer {

            guard let touchView = touch.view else {
                return true
            }

            return (!touchView.isFirstResponder || (self.isKind(of: UITextView.self) && self.isEqual(touchView)))
        }
        else {
            return true
        }
    }

    func panGestureDidChange(_ gesture: UIPanGestureRecognizer) {

        if self.keyboardActiveInput == nil {
            self.keyboardActiveInput = self.recursiveFindFirstResponder(self)
        }

        guard let hostView = self.findInputSetHostView() else {
            return
        }
        guard let activeView = self.keyboardActiveView else {
            return
        }
        guard let hostViewSuperView = hostView.superview else {
            return
        }

        let keyboardViewHeight:   CGFloat = hostView.bounds.size.height
        let keyboardWindowHeight: CGFloat = hostViewSuperView.bounds.size.height
        let touchLocationInKeyboardWindow = gesture.location(in: hostView.superview)

        if touchLocationInKeyboardWindow.y > keyboardWindowHeight - keyboardViewHeight - self.keyboardTriggerOffset {
            hostView.isUserInteractionEnabled = false
        }
        else {
            hostView.isUserInteractionEnabled = true
        }

        switch gesture.state {
            case .began:

                gesture.maximumNumberOfTouches = gesture.numberOfTouches
                break

            case .changed:
                var newKeyboardViewFrame = hostView.frame
                newKeyboardViewFrame.origin.y = touchLocationInKeyboardWindow.y + self.keyboardTriggerOffset
                newKeyboardViewFrame.origin.y = min(newKeyboardViewFrame.origin.y, keyboardWindowHeight)
                newKeyboardViewFrame.origin.y = max(newKeyboardViewFrame.origin.y, keyboardWindowHeight - keyboardViewHeight)

                if newKeyboardViewFrame.origin.y != hostView.frame.origin.y {

                    UIView.animate(withDuration: 0,
                                   delay: 0,
                                   options: .beginFromCurrentState,
                                   animations: {

                                       hostView.frame = newKeyboardViewFrame
                                       activeView.frame = newKeyboardViewFrame
                                   },
                                   completion: {
                                       finished in
                                   })
                }

                break

            case .ended, .cancelled:

                let thresholdHeight = keyboardWindowHeight - keyboardViewHeight - self.keyboardTriggerOffset + 44
                let velocity        = gesture.velocity(in: hostView)
                var shouldRecede    = Bool()

                if touchLocationInKeyboardWindow.y < thresholdHeight || velocity.y < 0 {
                    shouldRecede = false
                }
                else {
                    shouldRecede = true
                }

                var newKeyboardViewFrame = hostView.frame
                newKeyboardViewFrame.origin.y = !shouldRecede ? keyboardWindowHeight - keyboardViewHeight : keyboardWindowHeight

                UIView.animate(withDuration: 0.25,
                               delay: 0,
                               options: [.curveEaseOut, .beginFromCurrentState],
                               animations: {
                                   hostView.frame = newKeyboardViewFrame
                                   activeView.frame = newKeyboardViewFrame
                               }, completion: {
                    (finished: Bool) in

                    if shouldRecede {
                        UIView.setAnimationsEnabled(false)
                        self.hideKeyboard()
                        UIView.setAnimationsEnabled(true)
                    }

                    hostView.isUserInteractionEnabled = !shouldRecede
                })
                gesture.maximumNumberOfTouches = LONG_MAX

                break
            default:
                break
        }
    }

    // MARK: - Internal Methods

    func findInputSetHostView() -> UIView? {

        if #available(iOS 9, *) {

            guard let remoteKeyboardWindowClass = NSClassFromString("UIRemoteKeyboardWindow") else { return nil }
            guard let inputSetHostViewClass = NSClassFromString("UIInputSetHostView") else { return nil }

            for window in UIApplication.shared.windows {
                if window.isKind(of: remoteKeyboardWindowClass) {
                    for subView in window.subviews {
                        if subView.isKind(of: inputSetHostViewClass) {
                            for subSubView in subView.subviews {
                                if subSubView.isKind(of: inputSetHostViewClass) {
                                    return subSubView
                                }
                            }
                        }
                    }
                }
            }
        }
        else {
            return self.keyboardTrackView?.superview
        }

        return nil
    }

    func recursiveFindFirstResponder(_ view: UIView) -> UIView? {

        if view.isFirstResponder {
            return view
        }
        var found: UIView? = nil
        for v in view.subviews {

            found = self.recursiveFindFirstResponder(v)
            if found != nil {
                break
            }
        }
        return found
    }

    public func setInputView(_ inputView: UIView) {

        let trackView: KeyboardTrackView = KeyboardTrackView(frame: CGRect.zero)
        trackView.backgroundColor = .clear
        trackView.positionChangedCallback = {
            [weak self] frame in

            guard let s = self else {
                return
            }

            let fixFactor  = (s.keyboardTrackView?.frame.size.height ?? 0)
            let fixedFrame = CGRect(x: frame.origin.x,
                                    y: frame.origin.y + fixFactor,
                                    width: frame.size.width,
                                    height: frame.size.height - fixFactor)

            s.keyboardDidMoveBlock?(fixedFrame, false, false)
        }
        trackView.preferredSize = CGSize(width: self.frame.size.width, height: self.keyboardTriggerOffset)

        if self.keyboardTrackView != nil {
            self.keyboardTrackView?.positionChangedCallback = nil
            self.keyboardTrackView = nil
        }

        if inputView.inputAccessoryView == nil {
            if let textField = inputView as? UITextField {
                textField.inputAccessoryView = trackView
                self.keyboardTrackView = trackView
            }
            else if let textView = inputView as? UITextView {
                textView.inputAccessoryView = trackView
                self.keyboardTrackView = trackView
            }
        }
    }
}
